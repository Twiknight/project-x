import { Component, HostBinding, OnInit } from "@angular/core";
import { Router } from "@angular/router";
import {
  trigger,
  state,
  style,
  animate,
  transition
} from '@angular/animations';
import { ScrollService } from "../scroll.service";

@Component({
  styleUrls: ['./home.component.scss'],
  templateUrl: './home.component.html',
  animations: [trigger('routeChange', [
    transition('* => fadeIn', [
      style({ opacity: 0 }),
      animate(1000, style({ opacity: 1 }))
    ]),
    transition('* => fadeOut', [
      animate(1000, style({ opacity: 0 }))
    ])
  ])]
})
export class HomeComponent implements OnInit {
  @HostBinding('@routeChange') anmiation = '';
  constructor(private router: Router, private scrollService: ScrollService) {}

  ngOnInit() {
    this.scrollService.scrolled$
      .filter((x: MouseWheelEvent) => x.deltaY > 0)
      .do(() => {
        this.router.navigateByUrl('gallery')
      })
      .subscribe()
  }
}
