import { Injectable } from "@angular/core";
import { Observable } from "rxjs/Rx";

@Injectable()
export class ScrollService {
  scrolled$: Observable<MouseWheelEvent>;

  constructor() {
    this.scrolled$ = Observable.fromEvent(window, 'mousewheel', {passive: true})
      .throttleTime(1000)
      .share() as Observable<MouseWheelEvent>
  }
}
