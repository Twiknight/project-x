import { Component, OnInit } from "@angular/core";
import { Http } from "@angular/http";
import { Router } from "@angular/router";
import { environment } from "environments/environment";
import { ScrollService } from "../scroll.service";
import { Illustration } from "../illustration.model";

@Component({
  templateUrl: "./gallery.component.html",
  styleUrls: ["./gallery.component.scss"]
})
export class GalleryComponent implements OnInit {
  illustrations1: Illustration[];
  illustrations2: Illustration[];
  illustrations3: Illustration[];
  constructor(private router: Router, 
    private scrollService: ScrollService,
    private http: Http
  ) {}

  ngOnInit() {
    this.scrollService.scrolled$
      .filter(x => x.deltaY < 0 && document.documentElement.scrollHeight === 0)
      .do(() => {
        this.router.navigateByUrl('home')
      })
      .subscribe()
    this.http.get(environment.api)
      .map(resp => resp.json())
      .do(json => {
        this.illustrations1 = json['data']['illustrations1']
        this.illustrations2 = json['data']['illustrations2']
        this.illustrations3 = json['data']['illustrations3']
      })
      .subscribe();
  }
}
