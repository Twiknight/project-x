export type Illustration = {
  "category": number;
  "desc": string;
  "illustrationId": number;
  "name": string;
  "originUrl": string;
  "showType": number;
  "thumbnailUrl": string;
}